package ie.dkit.webframeworks.ca1;

/* DO NOT MODIFY THIS FILE! */

public class ObjectNotFoundException extends Exception {

    public ObjectNotFoundException() {
    }
    
    public ObjectNotFoundException(String msg) {
        super(msg);
    }
    
    public ObjectNotFoundException(Exception e) {
        super(e);
    }
}
