
package ie.dkit.webframeworks.ca1;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author lee
 */
public class InterpreterImpl implements Interpreter {

    //PG: careful here about access modifiers - best to follow Java convention of including them.
    HashMap<String, Object> map = new HashMap<>();
    HashMap<HashMap<String, Object>, Object> map2 = new HashMap<>();
    List<String> list;

    public InterpreterImpl() {
        this.map = new HashMap<>();
    }

    @Override
    public void createObject(String name, String type) throws ClassNotFoundException, MethodExecutionException, DuplicateObjectException, InvalidObjectNameException, NoSuchMethodException {

        if (map.containsKey(name)) {
            throw new DuplicateObjectException();
        }

        if (!name.matches("[a-zA-z0-9]+") || name.matches("(\\d)") || name.equalsIgnoreCase("delete") || name.isEmpty() || name.equalsIgnoreCase("new")) {
            throw new InvalidObjectNameException();
        } else {
            try {
                this.map.put(name, Class.forName(type).getConstructor().newInstance());
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            }
        }
    }

    @Override
    public void createObject(String name, String type, String[] args) throws ClassNotFoundException, MethodExecutionException, DuplicateObjectException, InvalidObjectNameException, NoSuchMethodException {
        Class[] c = new Class[args.length];

        for (int i = 0; i < args.length; i++) {
            c[i] = args[i].getClass();
            if (c[i].equals(Integer.class)) {
                c[i] = Integer.TYPE;
            }
        }
        try {
            //PG: no harm being explicit about whether a varargs or non-varargs call. technically grand as-is but helps someone reading it.
            Object obj = Class.forName(type).getConstructor(c).newInstance(args);
            map.put(name, obj);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {

        }

    }

    @Override
    public void deleteObject(String name) throws ObjectNotFoundException {

        if (map.containsKey(name)) {
            map.remove(name);
        } else {
            throw new ObjectNotFoundException();
        }
    }

    @Override
    public String callMethod(String name, String methodName) throws ObjectNotFoundException, NoSuchMethodException, MethodExecutionException {

        Object obj = map.get(name);
        try {
            return (String) obj.getClass().getMethod(methodName).invoke(obj);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new ObjectNotFoundException(e);
        }

    }

    @Override
    public String callMethod(String name, String methodName, String[] args) throws ObjectNotFoundException, NoSuchMethodException, MethodExecutionException {
        Class[] c = new Class[args.length];

        for (int i = 0; i < args.length; i++) {
            c[i] = args[i].getClass();

            if (c[i].equals(Integer.class)) {
                c[i] = Integer.TYPE;
            }

        }
        try {
            return (String) map.get(name).getClass().getMethod(methodName, c).invoke(map.get(name), (Object[]) args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new ObjectNotFoundException(e);
        }
    }

    @Override
    public List<String> objectNamesOfType(String type) {
        //PG: best not to hide fields with local variables! 
        ArrayList<String> list = new ArrayList<>();
        for (String key : map.keySet()) {
            try {
                //PG: why do you need to create a new instance here??
                if (map.get(key).equals(Class.forName(type).newInstance())) {
                    list.add(key);
                }
                //PG: this catch block is not only empty but probably unnecessary too.
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            }
        }
        return list;

    }

    @Override
    public List<String> objectNames() {
        //PG: again, best not to hide fields with local variables! 
        ArrayList list = new ArrayList(Arrays.asList(map.keySet().toArray()));
        return list;
    }

}
