package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;
import java.util.Scanner;

public class DispatcherDemo extends DispatcherImpl {

    public static void main(String[] args) {

        DispatcherDemo theApp = new DispatcherDemo();
        try {
            theApp.start();
        } catch (MalformedURLException | CommandDispatchException e) {
            System.out.println("Failure");
        }
    }

    private void start() throws MalformedURLException, CommandDispatchException {
        Dispatcher dispatcher = new DispatcherImpl();
        //PG: again, avoid hiding fields w/ local variables
        Interpreter interpreter = new InterpreterImpl();

        dispatcher.setInterpreter(interpreter);
        Scanner kb = new Scanner(System.in);

        System.out.println("Enter command or -1 to exit\n");
        String userInput = kb.nextLine();

        while (!"-1".equals(userInput)) {
            kb = new Scanner(System.in);
            try {
                String dispatchedCommand = dispatcher.dispatchCommand(userInput);
                System.out.println(dispatchedCommand);
                System.out.println("Enter command or -1 to exit\n");
                userInput = kb.nextLine();
            } catch (MalformedURLException | CommandDispatchException e) {
                       // e.printStackTrace();
                //System.err.println(e);
            }

        }

    }
}
