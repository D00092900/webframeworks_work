package ie.dkit.webframeworks.ca1;

/* DO NOT MODIFY THIS FILE! */

public class MethodExecutionException extends Exception {

    public MethodExecutionException() {
    }
    
    public MethodExecutionException(String msg) {
        super(msg);
    }
    
    public MethodExecutionException(Exception e) {
        super(e);
    }
}
