package ie.dkit.webframeworks.ca1;

import java.util.List;

/* DO NOT MODIFY THIS FILE! */

public interface Interpreter {
    
    public void createObject(String name, String type)
            throws
            ClassNotFoundException, 
            MethodExecutionException, 
            DuplicateObjectException, 
            InvalidObjectNameException,
            NoSuchMethodException
            ;
    
    public void createObject(String name, String type, String[] args)
            throws
            ClassNotFoundException, // thrown if class name not found
            MethodExecutionException, // wraps any exception thrown by constructor
            DuplicateObjectException,  // thrown if object already exists
            InvalidObjectNameException, // thrown if name not acceptable
            NoSuchMethodException // thrown if constructor can't be found
            ;
    
    public void deleteObject(String name) throws ObjectNotFoundException;
    
    public String callMethod(String name, String methodName)
            throws 
            ObjectNotFoundException, 
            NoSuchMethodException,
            MethodExecutionException
            ;
    
    public String callMethod(String name, String methodName, String[] args)
            throws 
            ObjectNotFoundException, 
            NoSuchMethodException,
            MethodExecutionException
            ;
    
    public List<String> objectNamesOfType(String type);
    
    public List<String> objectNames();
}
