package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;

public interface Dispatcher {
    
    public void setInterpreter(Interpreter interpreter);
    public String dispatchCommand(String command) throws MalformedURLException, CommandDispatchException;
    
}
