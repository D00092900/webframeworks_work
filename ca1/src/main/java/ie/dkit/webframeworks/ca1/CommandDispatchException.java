package ie.dkit.webframeworks.ca1;

/* DO NOT MODIFY THIS FILE! */

public class CommandDispatchException extends Exception {

    public CommandDispatchException() {
    }
    
    public CommandDispatchException(String msg) {
        super(msg);
    }
    
    public CommandDispatchException(Exception e) {
        super(e);
    }
}
