package ie.dkit.webframeworks.ca1;

/* DO NOT MODIFY THIS FILE! */

public class InvalidObjectNameException extends Exception {

    public InvalidObjectNameException() {
    }

    public InvalidObjectNameException(String msg) {
        super(msg);
    }
    
    public InvalidObjectNameException(Exception e) {
        super(e);
    }
}
