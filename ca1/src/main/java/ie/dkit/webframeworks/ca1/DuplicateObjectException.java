package ie.dkit.webframeworks.ca1;

/* DO NOT MODIFY THIS FILE! */

public class DuplicateObjectException extends Exception {


    public DuplicateObjectException() {
    }

    public DuplicateObjectException(String msg) {
        super(msg);
    }
    
    public DuplicateObjectException(Exception e) {
        super(e);
    }
}
