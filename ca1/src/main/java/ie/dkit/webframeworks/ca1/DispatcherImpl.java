
package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;
import java.util.List;

public class DispatcherImpl implements Dispatcher {

    Interpreter interpreter;

    @Override
    public void setInterpreter(Interpreter interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public String dispatchCommand(String command) throws MalformedURLException, CommandDispatchException {

        //PG: would be no harm making these final.
        String deleteRegex = "\\/delete\\?name\\=([a-zA-Z]+)";
        String newUrlRegex = "\\/new\\/([a-zA-Z\\.]+)\\/([a-zA-Z\\d]+)\\/([a-zA-Z\\d\\.*]+)";
        String objectRegex = "\\/objects";
        String filterRegex = "\\/objects\\/([a-zA-Z]+\\..*)";
        String methodParamRegex = "\\/([a-zA-Z\\d]+)\\/([a-zA-Z\\d]+)\\/(.*)";
        String methodRegex = "\\/([a-zA-Z]+)\\/([a-zA-Z]+)";

        //PG: avoid long if/else blocks for action selection. better to try something like a command pattern instead, avoids spaghetti code.
        if (RegexUtility.matches(command, objectRegex)) {
            try {
                List<String> strs = interpreter.objectNames();
                String listStr = String.join("/", strs);
                return listStr;
            } catch (Exception e) {
            }
        } else if (RegexUtility.matches(command, filterRegex)) {
            try {
                List<String> strs = interpreter.objectNamesOfType(RegexUtility.getFirst(command, filterRegex, 1));
                String listStr = String.join("/", strs);
                return listStr;
            } catch (Exception e) {

            }
        } else if (RegexUtility.matches(command, deleteRegex)) {
            try {
                interpreter.deleteObject(RegexUtility.getFirst(command, deleteRegex, 1));
                return "Deleted object " + RegexUtility.getFirst(command, deleteRegex, 1);
            } catch (ObjectNotFoundException e) {
            }
        } else if (RegexUtility.matches(command, methodRegex)) {
            try {
                return interpreter.callMethod(RegexUtility.getFirst(command, methodRegex, 1), RegexUtility.getFirst(command, methodRegex, 2));
            } catch (ObjectNotFoundException | NoSuchMethodException | MethodExecutionException e) {

            }
        } else if (RegexUtility.matches(command, methodParamRegex)) {
            try {
                return interpreter.callMethod(RegexUtility.getFirst(command, methodParamRegex, 1), RegexUtility.getFirst(command, methodParamRegex, 2), (RegexUtility.getFirst(command, methodParamRegex, 3)).split("/"));
            } catch (ObjectNotFoundException | NoSuchMethodException | MethodExecutionException e) {

            }
        } else if (RegexUtility.matches(command, newUrlRegex)) {
            try {
                interpreter.createObject(RegexUtility.getFirst(command, newUrlRegex, 2), RegexUtility.getFirst(command, newUrlRegex, 1), (RegexUtility.getFirst(command, newUrlRegex, 3)).split("/"));
                return "Create Object " + RegexUtility.getFirst(command, newUrlRegex, 2);
            } catch (ClassNotFoundException | MethodExecutionException | DuplicateObjectException | InvalidObjectNameException | NoSuchMethodException e) {
            }
        } else if(!RegexUtility.matches(command, "^([\\/])"))throw new MalformedURLException();{
            
            return "Invalid!!";
            
        }
    }
}
