package ie.dkit.webframeworks.ca1;

import java.net.MalformedURLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import org.mockito.exceptions.verification.NeverWantedButInvoked;
import org.mockito.exceptions.verification.WantedButNotInvoked;

/* DO NOT MODIFY THIS FILE */
/*
This file uses a technique called "mocking" to make a mock of the interpreter,
allowing the tests to verify that specific methods are called, rather than 
having to call the methods and check the result.

This is a good way to test different interconnected classes, since it allows
the testing to be isolated to the particular class under test, examining its
expected interactions with other objects without needing to instantiate the
objects themselves.
*/

public class DispatcherTest {
    
    Dispatcher dispatcher;
    Interpreter interpreter;
    
    @Before
    public void setup() {
        dispatcher = new DispatcherImpl();
        interpreter = Mockito.mock(Interpreter.class);
        dispatcher.setInterpreter(interpreter);
    }
    
    @After
    public void teardown() {
        dispatcher = null;
        interpreter = null;
    }
    
    @Test(expected=MalformedURLException.class)
    public void invalidUrlThrowsException() throws CommandDispatchException, MalformedURLException {
        dispatcher.dispatchCommand("wxyz");
    }
    
    @Test
    public void invalidUrlDoesNotInvokeInterpreter() throws CommandDispatchException, MalformedURLException {
        try {
            dispatcher.dispatchCommand("wxyz");
        } catch ( MalformedURLException | CommandDispatchException e ) {
            Mockito.verifyZeroInteractions(interpreter);
        }
    }
        
    @Test
    public void newUrlRunsNewCommand() throws Exception {
        dispatcher.dispatchCommand("/new/java.lang.String/myString/hello");
        Mockito.verify(interpreter).createObject("myString", "java.lang.String", new String[] { "hello" });
    }
    
    @Test
    public void deleteUrlRunsDeleteCommand() throws Exception {
        dispatcher.dispatchCommand("/delete?name=myString");
        Mockito.verify(interpreter).deleteObject("myString");
    }
    
    @Test
    public void objectsUrlRunsObjectsCommand() throws Exception {
        dispatcher.dispatchCommand("/objects");
        Mockito.verify(interpreter).objectNames();
    }
    
    @Test
    public void objectsUrlWithFilter() throws Exception {
        dispatcher.dispatchCommand("/objects/java.lang.String");
        Mockito.verify(interpreter).objectNamesOfType("java.lang.String");
    }
    
    @Test
    public void runMethodWithoutParams() throws Exception {
        dispatcher.dispatchCommand("/myPerson/setFirstnameAndSurname");        
        try {
            verify(interpreter, never()).callMethod("myPerson", "setFirstnameAndSurname", new String[] {});
            verify(interpreter, never()).callMethod("myPerson", "setFirstnameAndSurname");
            // will not get to here unless haven't called either method.
            throw new WantedButNotInvoked("neither version invoked");
        }
        catch (NeverWantedButInvoked ignored) {
            // expect since one or the other will be called
            // the "never" makes sure to break out of the try block. 
        }
    }
    
    @Test
    public void runMethodWithParams() throws Exception {
        dispatcher.dispatchCommand("/myPerson/setFirstnameAndSurname/John/Byrne");
        Mockito.verify(interpreter).callMethod("myPerson", "setFirstnameAndSurname", new String[] { "John", "Byrne" });
    }
    
}
