package ie.dkit.webframeworks.ca1;

import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class InterpreterTest {
    
    private Interpreter interpreter;
    
    @Before
    public void setup() {
        interpreter = new InterpreterImpl();
    }
    
    @After
    public void teardown() {
        interpreter = null;
    }
    
    @Test
    public void isImplemented() throws ClassNotFoundException {
        Class.forName("ie.dkit.webframeworks.ca1.InterpreterImpl");
    }
    
    @Test
    public void duplicateObjectNameRefused() throws Exception {
        String objectName = "x";
        String className = "java.lang.String";
        for ( int x = 0 ; x < 2 ; ++x ) {
            try {
                interpreter.createObject(objectName, className);
            } catch ( DuplicateObjectException e ) {
                assertEquals(1, x);
            }
        }
    }
    
    @Test(expected=InvalidObjectNameException.class)
    public void objectNameEmptyStringRefused() throws Exception {
        interpreter.createObject("", "java.lang.String");
    }
    
    @Test(expected=InvalidObjectNameException.class)
    public void objectNameCantBeANumber() throws Exception {
        interpreter.createObject("4", "java.lang.String");
    }
    
    @Test(expected=InvalidObjectNameException.class)
    public void objectNameMustBeAlphanumeric() throws Exception {
        interpreter.createObject("xyz-_+#@", "java.lang.String");
    }
    
    @Test(expected=InvalidObjectNameException.class)
    public void objectNameCannotBeNew() throws Exception {
        interpreter.createObject("new", "java.lang.String");
    }
    
    @Test(expected=InvalidObjectNameException.class)
    public void objectNameCannotBeDelete() throws Exception {
        interpreter.createObject("delete", "java.lang.String");
    }
    
    @Test
    public void looksUpEmptyListOfAllIfNoObjects() throws Exception {
        List<String> matches = interpreter.objectNames();
        assertEquals(0, matches.size());
    }
    
    @Test
    public void looksUpEmptyListOfTypeIfNoObjects() throws Exception {
        interpreter.createObject("onfe", "ie.dkit.webframeworks.ca1.ObjectNotFoundException");
        List<String> matches = interpreter.objectNamesOfType("java.lang.String");
        assertEquals(0, matches.size());
    }
    
    @Test
    public void looksUpAllObjects() throws Exception {
        interpreter.createObject("s1", "java.lang.String");
        interpreter.createObject("s2", "java.lang.String");
        interpreter.createObject("onfe", "ie.dkit.webframeworks.ca1.ObjectNotFoundException");
        List<String> matches = interpreter.objectNames();
        assertTrue(matches.contains("s1"));
        assertTrue(matches.contains("s2"));
        assertTrue(matches.contains("onfe"));
    }
    
    @Test
    public void looksUpObjectsOfGivenType() throws Exception {
        interpreter.createObject("s1", "java.lang.String");
        interpreter.createObject("s2", "java.lang.String");
        interpreter.createObject("onfe", "ie.dkit.webframeworks.ca1.ObjectNotFoundException");
        List<String> matches = interpreter.objectNamesOfType("java.lang.String");
        assertTrue(matches.contains("s1"));
        assertTrue(matches.contains("s2"));
        assertTrue(!matches.contains("onfe"));
    }
    
    @Test
    public void canCreateAndReturnString() throws Exception {
        String testString = "Hello";
        String expectedResult = testString.toUpperCase();
        interpreter.createObject("s1", "java.lang.String", new String[] {testString});
        String output = interpreter.callMethod("s1", "toUpperCase");
        assertEquals(output, expectedResult);
    }
    
    @Test
    public void canCreateWith2ArgumentConstructor() throws Exception {
        interpreter.createObject("john", "ie.dkit.webframeworks.ca1.Person", new String[] {"John", "Smith"});
        String output = interpreter.callMethod("john", "getFullname");
        assertEquals("Smith, John", output);
    }
    
    @Test
    public void canCallOverloadedNoArgumentMethod() throws Exception {
        interpreter.createObject("taoiseach", "ie.dkit.webframeworks.ca1.Person", new String[] {"Enda", "Kenny"});
        interpreter.callMethod("taoiseach", "setFirstnameAndSurname");
        String output = interpreter.callMethod("taoiseach", "getFullname");
        assertEquals("Doe, John", output);
    }
    
    @Test
    public void canCall2ArgumentMethod() throws Exception {
        interpreter.createObject("taoiseach", "ie.dkit.webframeworks.ca1.Person", new String[] {"Enda", "Kenny"});
        interpreter.callMethod("taoiseach", "setFirstnameAndSurname", new String[] {"Margaret", "Thatcher"});
        String output = interpreter.callMethod("taoiseach", "getFullname");
        assertEquals("Thatcher, Margaret", output);
    }
    
    @Test
    public void canCreateAndReturnInteger() throws Exception {
        String testString = "3";
        String expectedResult = new Integer(testString).toString();
        interpreter.createObject("s1", "java.lang.Integer", new String[] {testString});
        String output = interpreter.callMethod("s1", "toString");
        assertEquals(output, expectedResult);
    }
    
}