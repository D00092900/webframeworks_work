package ie.dkit.webframeworks.exceptionslab;

/**
 * Exercise 2: Pet Animal
 * 
 * Our PetAnimal is checked on every hour.
 * The animal care class has the job of looking at the exception thrown and
 * reporting if everything is OK.
 * 
 * If the HungryException is thrown, return "the animal is hungry".
 * If the ThirstyException is thrown, return "the animal needs a drink".
 * If the FrightenedException is thrown, don't handle it, but pass it up.
 */
public class Exercise2AnimalCare {
    
    private final PetAnimal cat = new PetAnimal();
    
    public String statusAt(int hour) throws FrightenedException{
        try {
            cat.checkin(hour);
            return "everything is OK";
        } catch (HungryException e) {
            return "the animal is hungry";
        }catch (ThirstyException e){
            return "the animal needs a drink";
        }
    }
    
}
