package ie.dkit.webframeworks.exceptionslab;

/*
 This class implements a basic exam outcome calculator.

 It should return ExamOutcome.PASS is the mark is greater than or equal to 40.
 Otherwise it should pass.

 Modify the body of the the outcomeForMark method to throw a runtime/unchecked
 exception of type IllegalArgumentException 
 with the message "out of range 0-100" when a mark below 0 or
 above 100 is input.

 */
public class Exercise1ExamCalculator {

    public ExamOutcome outcomeForMark(int mark) {
        final int PASSMARK = 40;
        final int MAXIMUM_MARK = 100;
        final int MINIMUM_MARK = 0;
        if (mark > MAXIMUM_MARK || mark < MINIMUM_MARK) {
            throw new IllegalArgumentException("not complete yet");
        } else if (mark >= PASSMARK) {
            return ExamOutcome.PASS;
        } else {
            return ExamOutcome.FAIL;
        }
    }

}
