import ie.dkit.webframeworks.propertieslab.DefaultPropertyReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class DefaultPropertyReaderTest {
    
    /*
    In case you're interested:
    
    Sometimes we actually want to repeat the same unit test on a number of
    different pieces of data.  Ideally we won't be wanting to break the DRY
    principle by copying the same unit test, however we do want each test to 
    appear separately in the test runner.  The solution is the Parameterised
    unit test.  It uses a static method to initialise the test class via its
    constructor, see below for details: 
    */
    
    @Parameters(name = "{index}: {0}={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                { "site.title", "Course website" },
                { "site.subtitle", "Dundalk Institute of Technology" }, 
                { "site.country", "Ireland" },
                { "contact.number", "don't know" }
           });
    }
    
    private String input, expected;
    
    public DefaultPropertyReaderTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }
    
    private DefaultPropertyReader propertyReader;
    
    @Before
    public void setup() {
        propertyReader = new DefaultPropertyReader();
    }
    
    @Test
    public void readsPropertyCorrectly() throws IOException {
        assertEquals(this.expected, propertyReader.getPropertyFromFile(this.input));
    }
    
    @After
    public void teardown() {
        propertyReader = null;
    }
    
}
