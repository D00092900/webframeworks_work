package ie.dkit.webframeworks.propertieslab;

import java.io.IOException;
import java.util.Properties;

/*
In this exercise, subclass the PropertyReader and Override the get method
so that it returns the string "don't know" for any property whose value is 
unknown. 
*/

public class DefaultPropertyReader extends PropertyReader {
        public String getPropertyFromFile(String propertyName) throws IOException {
        Properties p = new Properties();
        p.load(getClass().getResourceAsStream("/site.properties"));
        return (p.getProperty(propertyName,"don't know"));
        
    }
}
