package ie.dkit.webframeworks.propertieslab;

import java.io.IOException;
import java.util.Properties;

/*
Complete the method below, looking up properties from the file site.properties
in the default package.

The amount of code needed to do this is quite small, so normally you'd just
set this up directly in your program, rather than abstracting it into a class,
since the load step only needs to be done once to lookup properties.

*/

public class PropertyReader {
    
    public String getPropertyFromFile(String propertyName) throws IOException {
        Properties p = new Properties();
        p.load(getClass().getResourceAsStream("/site.properties"));
        return (p.getProperty(propertyName));
        
    }
    
}
