package ie.dkit.webframeworks.reflection1lab;

public class Person {
    
    protected String firstname;
    protected String surname;
    
    public Person() {
        this.firstname = "John";
        this.surname = "Doe";
    }
    
    public Person(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
    }
    
    public void setFirstnameAndSurname() {
        this.firstname = "John";
        this.surname = "Doe";
    }
    
    public void setFirstnameAndSurname(String firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname; 
    }
    
    public String getFullname() {
        return String.format("%s, %s", surname, firstname);
    }
    
}
