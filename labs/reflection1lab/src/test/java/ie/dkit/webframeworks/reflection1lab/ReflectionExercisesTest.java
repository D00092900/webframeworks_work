package ie.dkit.webframeworks.reflection1lab;


import ie.dkit.webframeworks.reflection1lab.ReflectionExercises;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class ReflectionExercisesTest {
   
    ReflectionExercises exercises;
    
    @Before
    public void setup() {
        exercises = new ReflectionExercises();
    }
    
    @After
    public void teardown() {
        exercises = null;
    }
    
    @Test
    public void exercise1Works() {
        assertTrue(exercises.ex1DetermineClassName().contains("String"));
    }
    
    @Test
    public void exercise2Works() {
        String myString = "xyz";
        assertEquals("java.lang.String", exercises.ex2DetermineClassNameForAnyObject(myString));
    }
    
    @Test
    public void exercise3Works() {
        String myString = "xyz";
        assertEquals("String", exercises.ex3DetermineSimpleClassNameForAnyObject(myString));
    }
    
    @Test
    public void exercise4Works() throws InstantiationException, IllegalAccessException {
        Person p = new Person("Enda", "Kenny");
        Person p2 = (Person) exercises.ex4CreateANewInstance(p);
        assertEquals("Doe, John", p2.getFullname());
    }
    
    @Test
    public void exercise5Works() throws InstantiationException, IllegalAccessException {
        Politician p = new Politician("Enda", "Kenny", "Fine Gael");
        assertEquals("ie.dkit.webframeworks.reflection1lab.Person", exercises.ex5WhatsTheSuperclass(p));
    }
    
}
