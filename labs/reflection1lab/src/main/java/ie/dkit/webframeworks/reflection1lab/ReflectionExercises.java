/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.reflection1lab;

/**
 *
 * @author peadar
 */
public class ReflectionExercises {
    
    public String ex1DetermineClassName() {
        
        String myString = "this is a String.";
        
        // add in the necessary code here to return the fully-qualified
        // class name of the string here.
        
        return myString;
        
    }
    
    public String ex2DetermineClassNameForAnyObject(Object object) {
        
        // add in the necessary code here to return the fully-qualified
        // class name of the object passed in here.
        return object.getClass().getName();
        
        
    }
    
    public String ex3DetermineSimpleClassNameForAnyObject(Object object) {
        
        // add in the necessary code here to return the Simple
        // class name of the object passed in here.
        
        return object.getClass().getSimpleName();
        
    }
    
    public Object ex4CreateANewInstance(Object original) throws InstantiationException, IllegalAccessException {
        
        // add in the necessary code to create a new instance of the object
        // passed in, using the default constructor,
        // returning the new instance instead of the original.
        
        return original.getClass().newInstance();
        
    }
    
    public String ex5WhatsTheSuperclass(Object object) {
        
        // return the name of the superclass of the object passed in.
        
        return object.getClass().getSuperclass().getName();
        
    }
}
