package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;

public class DummyUrlParser implements UrlParser {

    @Override
    public ParsedUrl parse(String url) throws MalformedURLException {
        throw new UnsupportedOperationException("not implemented by student."); 
    }
    
    
    
}
