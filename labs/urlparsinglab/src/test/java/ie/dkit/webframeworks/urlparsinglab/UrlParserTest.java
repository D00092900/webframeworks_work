package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class UrlParserTest {
    
    UrlParser urlParser;
    boolean found = false;
    
    @Before
    public void setup() {
        try {
            urlParser = (UrlParser) Class.forName("ie.dkit.webframeworks.urlparsinglab.UrlParserImpl").newInstance();
            found = true;
        } catch ( ClassNotFoundException | InstantiationException | IllegalAccessException e ) {
            urlParser = new DummyUrlParser();
        }
    }
    
    @After
    public void teardown() {
        urlParser = null;
        found = false;
    }
    
    @Test
    public void isImplementedAsUrlParserImpl() {
        assertTrue(found);
    }
    
    @Test
    public void canParseProtcolAndHostname() throws MalformedURLException {
        String url = "http://www.dkit.ie/";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
    }
    
    @Test
    public void canParseHostnameWithoutTrailingSlash() throws MalformedURLException {
        String url = "http://www.dkit.ie";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
    }
    
    @Test
    public void canParseShallowPathWithoutTrailingSlash() throws MalformedURLException {
        String url = "http://www.dkit.ie/policies";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("policies", parsedUrl.getPath());
    }
    
    @Test
    public void canParseShallowPathWithSingleParameter() throws MalformedURLException {
        String url = "http://www.dkit.ie/policies?name=John";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("policies", parsedUrl.getPath());
        assertEquals("John", parsedUrl.getParameter("name"));
    }
    
    public void canParseShallowPathWithMultipleParameters() throws MalformedURLException {
        String url = "http://www.dkit.ie/policies?firstname=John&surname=Smith";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("policies", parsedUrl.getPath());
        assertEquals("John", parsedUrl.getParameter("name"));
        assertEquals("Smith", parsedUrl.getParameter("surname"));
    }
    
    @Test
    public void canParseShallowPathWithTrailingSlash() throws MalformedURLException {
        String url = "http://www.dkit.ie/policies/";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("policies/", parsedUrl.getPath());
    }
    
    @Test
    public void canParseDeepPathWithoutTrailingSlash() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("about/policies", parsedUrl.getPath());
    }
    
    @Test
    public void canParseDeepPathWithTrailingSlash() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies/";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("about/policies/", parsedUrl.getPath());
    }
    
    @Test
    public void canParseDeepPathWithSingleParameter() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies?name=John";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("about/policies", parsedUrl.getPath());
        assertEquals("John", parsedUrl.getParameter("name"));
    }
    
    @Test
    public void canParseDeepPathWithMultipleParameters() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies?firstname=John&surname=Smith";
        ParsedUrl parsedUrl = urlParser.parse(url);
        assertEquals("http", parsedUrl.getProtocol());
        assertEquals("www.dkit.ie", parsedUrl.getHost());
        assertEquals("about/policies", parsedUrl.getPath());
        assertEquals("John", parsedUrl.getParameter("name"));
        assertEquals("Smith", parsedUrl.getParameter("surname"));
    }
    
    @Test(expected=MalformedURLException.class)
    public void twoQuestionMarksReportedAsMalformed() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies?firstname=John?surname=Smith";
        urlParser.parse(url);
    }
    
    @Test(expected=MalformedURLException.class)
    public void noQuestionMarkWithParametersReportedAsMalformed() throws MalformedURLException {
        String url = "http://www.dkit.ie/about/policies&firstname=John&surname=Smith";
        urlParser.parse(url);
    }
    
    @Test(expected=MalformedURLException.class)
    public void missingProtocolReportedAsMalformed() throws MalformedURLException {
        String url = "//www.dkit.ie/about/policies&firstname=John&surname=Smith";
        urlParser.parse(url);
    }
}
