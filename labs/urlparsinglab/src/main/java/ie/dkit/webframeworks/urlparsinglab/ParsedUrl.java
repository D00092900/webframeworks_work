package ie.dkit.webframeworks.urlparsinglab;

public interface ParsedUrl {
    
    public String getProtocol();
    public String getHost();
    public String getPath();
    public String getParameter(String parameterName);
    
}
