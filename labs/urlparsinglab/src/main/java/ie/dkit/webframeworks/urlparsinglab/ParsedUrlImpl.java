package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;
import java.net.URL;
/**
 *
 * @author lee
 */
public class ParsedUrlImpl implements ParsedUrl{
    String protocol;
    String host;
    String path;
    String parameter;
    String url;


    ParsedUrlImpl(String url, String parameterName) throws MalformedURLException {
        this.url = url;
        URL u = new URL(url);
        
        this.protocol = u.getProtocol();
        this.host = u.getHost();
        this.path = u.getHost();
        String query = u.getQuery();
        
        this.parameter =(getParameter(parameterName));
    }

    @Override
    public String getProtocol() {
      return this.protocol;
    }

    @Override
    public String getHost() {
        return this.host;
    }

    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public String getParameter(String parameterName) {
     
    return this.parameter;
    }
    
}
