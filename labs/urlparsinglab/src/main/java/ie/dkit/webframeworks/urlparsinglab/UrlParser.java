package ie.dkit.webframeworks.urlparsinglab;

import java.net.MalformedURLException;

public interface UrlParser {
    
    public ParsedUrl parse(String url) throws MalformedURLException;
    
}
