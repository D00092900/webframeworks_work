package ie.dkit.webframeworks.reflection2lab;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class MethodsListerTest {
    
    MethodsLister methodsLister;
    
    @Before
    public void setup() {
        methodsLister = new MethodsLister();
    }
    
    @After
    public void teardown() {
        methodsLister = null;
    }
    
    @Test
    public void allMethodsSupportedForPersonClass() {
        ArrayList<String> names = methodsLister.allMethodsSupportedFor(new Person());
        assertTrue(names.contains("fullname"));
        assertTrue(names.contains("getClass"));
    }
    
    @Test
    public void allMethodsSupportedForUrlClass() throws MalformedURLException {
        ArrayList<String> names = methodsLister.allMethodsSupportedFor(new URL("http://www.google.ie/"));
        assertTrue(names.contains("getQuery"));
        assertTrue(names.contains("getClass"));
    }
    
    @Test
    public void declaredMethodsSupportedForPersonClass() {
        ArrayList<String> names = methodsLister.declaredMethodsSupportedFor(new Person());
        assertTrue(names.contains("fullname"));
        assertFalse(names.contains("toString"));
    }
    
    @Test
    public void declaredMethodsSupportedForUrlClass() throws MalformedURLException {
        ArrayList<String> names = methodsLister.declaredMethodsSupportedFor(new URL("http://www.google.ie/"));
        assertTrue(names.contains("getQuery"));
        assertFalse(names.contains("getClass"));
    }
    
    
}
