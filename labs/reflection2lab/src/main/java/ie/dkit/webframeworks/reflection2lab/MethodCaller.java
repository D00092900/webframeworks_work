package ie.dkit.webframeworks.reflection2lab;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class MethodCaller {
    
    public Object callMethod(Object object, String methodName)
            throws Exception {
        
        /*
        Take in the object object and using reflection call its method
        "methodName", returning out any object that may come. 
        */
        Method m = object.getClass().getMethod(methodName);
        return m.invoke(object);
    }
    
    public Object callMethodWithParameters(
            Object object, 
            String methodName, 
            Object[] parameters) 
            throws Exception {
        
        /*
        This is a hard one!
        
        You need to look up the relevant method matching the signature from the
        types of the objects passed in. 
        Once you've found the relevant method, you then need to invoke it with
        the supplied parameters. 
       
        You may find it helpful to use the to/from array conversions in
        ArrayList to help you. 
        */
        ArrayList<Class> types = new ArrayList();
        for(Object parameter : parameters){
            types.add(parameter.getClass());
        }        
        
        
        return object.getClass().getMethod(methodName,types.toArray(new Class[]{})).invoke(object, parameters);
        
        
    }
    
}
