package ie.dkit.webframeworks.customexceptionslab;


// DO NOT MODIFY THIS FILE.

public interface WrapDemo {
    public void demonstrateWrapping() throws Exception;
    
}
