/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ie.dkit.webframeworks.customexceptionslab;

/**
 *
 * @author lee
 */
public class WrapDemoImpl implements WrapDemo{

    @Override
    public void demonstrateWrapping() throws Exception {
        throw new MyCheckedException(new MyRuntimeException("low level external problem")); 
    }
    
}
