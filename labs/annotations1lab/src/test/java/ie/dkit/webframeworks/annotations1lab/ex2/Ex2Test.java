package ie.dkit.webframeworks.annotations1lab.ex2;

import java.nio.Buffer;
import java.util.Set;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class Ex2Test {
    
    ClassFinder classFinder;
    
    @Before
    public void setup() {
        classFinder = new ClassFinder();
    }
    
    @Test
    public void canListClassesImplementingInterface() {
        Set<String> foundClasses = classFinder.classNamesInPackageExtending("ie.dkit.webframeworks.annotations1lab.ex2", Entity.class);
        // should be only 3 items, exactly: 
        assertEquals(3, foundClasses.size());
        assertThat(foundClasses, hasItem("Fine"));
        assertThat(foundClasses, hasItem("Person"));
        assertThat(foundClasses, hasItem("Vehicle"));
    }
    
    @Test
    public void canFindClassesInStandardJavaPackage() {
        Set<String> foundClasses = classFinder.classNamesInPackageExtending("java.nio", Buffer.class);
        assertThat(foundClasses, hasItem("IntBuffer"));
        assertThat(foundClasses, hasItem("FloatBuffer"));
        assertThat(foundClasses, hasItem("StringBuffer"));
    }
    
    @After
    public void teardown() {
        classFinder = null;
    }
}
