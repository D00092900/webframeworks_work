package ie.dkit.webframeworks.annotations1lab.ex2;

import org.reflections.Reflections;
import java.util.HashSet;
import java.util.Set;

public class ClassFinder {

    Set<String> classNamesInPackageExtending(String packageName, Class extendedType) {
        String path = "ie.dkit.webframeworks.annotations1lab.ex2";
        Reflections reflection = new Reflections(packageName);

        Set<String> names = new HashSet();
        Set<Class<?>> annotation = reflection.getSubTypesOf(extendedType);

        for (Class x : annotation) {
            names.add(x.getSimpleName());
        }

        return names;
    }

}
