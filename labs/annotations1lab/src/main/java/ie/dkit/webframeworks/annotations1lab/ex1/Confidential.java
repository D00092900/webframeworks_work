package ie.dkit.webframeworks.annotations1lab.ex1;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
        
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)

public @interface Confidential {
    
}
