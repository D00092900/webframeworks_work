package ie.dkit.webframeworks.unittestinglab;

public class FactorialCalculator {

    public int factorial(int n) throws ArithmeticException {
        if(n >= 0)
        {
            if (n == 0)
            {
                return 1;
            } 
            else
            {
                return n * factorial(n - 1);
            }
        }
        
        
        throw new ArithmeticException("complete this!");

    }

}
