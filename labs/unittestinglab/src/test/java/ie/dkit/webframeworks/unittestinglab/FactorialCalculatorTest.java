package ie.dkit.webframeworks.unittestinglab;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class FactorialCalculatorTest {
    
    FactorialCalculator calculator;
    
    @Before
    public void setup() {
        calculator = new FactorialCalculator();
    }
    
    @After
    public void tearDown() {
        calculator = null;
    }
    
    // test a "normal" usage case
    @Test
    public void computesPositiveFactorial() throws Exception {
        assertEquals(6, calculator.factorial(3));
    }

    // test an edge case
    @Test
    public void computesFactorialZero() throws Exception {
        assertEquals(1, calculator.factorial(0));
    }
    
    // test that an erroneous input is caught and rejected
    @Test(expected=ArithmeticException.class)
    public void throwsExceptionOnNegativeFactorial() {
        calculator.factorial(-1);
    }
    
}
