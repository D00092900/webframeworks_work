package ie.dkit.webframeworks.servletslab;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Assert;
import org.junit.Test;

public class CalculatorServletIT {
    
    WebClient webClient = new WebClient();
    
    protected String urlStem;
    public CalculatorServletIT() {
        urlStem = "http://localhost:8180/servletslab/CalculatorServlet?";
    }
    
    private String fullUrlForPath(String path) {
        return urlStem + path;
    }
    
    @Test
    public void addWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=add"));
        Assert.assertTrue(page.getBody().asText().contains("50"));
    }
    
    @Test
    public void subtractWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=subtract"));
        Assert.assertTrue(page.getBody().asText().contains("10"));
    }
    
    @Test
    public void multiplyWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=30&y=20&action=multiply"));
        Assert.assertTrue(page.getBody().asText().contains("600"));
    }
    
    @Test
    public void divideWorks() throws Exception {
        HtmlPage page = webClient.getPage(fullUrlForPath("x=3000&y=15&action=divide"));
        Assert.assertTrue(page.getBody().asText().contains("200"));
    }
    
    @Test
    public void divideWorksOnAnotherUrl() throws Exception {
        String alternateUrl = "http://localhost:8180/servletslab/ADifferentCalculatorServlet?x=3000&y=15&action=divide";
        HtmlPage page = webClient.getPage(alternateUrl);
        Assert.assertTrue(page.getBody().asText().contains("200"));
    }
    
}
