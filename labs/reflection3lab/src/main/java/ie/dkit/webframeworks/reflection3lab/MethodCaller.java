package ie.dkit.webframeworks.reflection3lab;

public class MethodCaller {
    
    public Object callMethodWithParameters(
            Object object, 
            String methodName, 
            Object[] parameters) 
            throws Exception {
        
        /*
        
        You are to look up the relevant method matching the signature from the
        types of the objects passed in. 
        Once you've found the relevant method, you then need to invoke it with
        the supplied parameters. 
        
        The catch is that you must convert the autoboxed types to their 
        primitive equivalents (assuming most methods are written with int as
        opposed to Integer for example.
       
        You may find it helpful to use the to/from array conversions in
        ArrayList to help you. 
        */
        
        return null;
    }
    
}
