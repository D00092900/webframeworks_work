package ie.dkit.webframeworks.reflection3lab;

public class ObjectConstructor {
    
    public Object constructObjectWithParameters(
            String className,
            Object[] parameters)
            throws Exception {
        
        /*
        
        You are to look up the relevant constructor matching the signature from the
        types of the objects passed in. 
        Once you've found the relevant constructor, you then need to invoke it with
        the supplied parameters. 
        
        As before, 
        the catch is that you must convert the autoboxed types to their 
        primitive equivalents (assuming most methods are written with int as
        opposed to Integer for example.
       
        You may find it helpful to use the to/from array conversions in
        ArrayList to help you. 
        */
        
        return null;
    }
    
}
