package ie.dkit.webframeworks.reflection3lab;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MethodCallerTest {

    private MethodCaller methodCaller;
    private Person mockPerson;
    
    @Before
    public void setup() {
        methodCaller = new MethodCaller();
        mockPerson = Mockito.mock(Person.class);
    }
    
    @After
    public void teardown() {
        methodCaller = null;
        mockPerson = null;
    }
    
    @Test
    public void correctCall1String() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "rename", new Object[] { "John", "Smith" });
        Mockito.verify(mockPerson).rename("John", "Smith");
    }
    
    @Test
    public void correctCall2Strings() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "rename", new Object[] {"Smith, John"});
        Mockito.verify(mockPerson).rename("Smith, John");
    }
    
    @Test
    public void correctCall1Int() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "assignSalaryPackage", new Object[] {50000});
        Mockito.verify(mockPerson).assignSalaryPackage(50000);
    }
    
    @Test
    public void correctCall2Ints() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "assignSalaryPackage", new Object[] {50000, 10000});
        Mockito.verify(mockPerson).assignSalaryPackage(50000, 10000);
    }
    
    @Test
    public void correctCall2Ints1String() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "assignSalaryPackage", new Object[] {50000, 10000, "VW Golf Mk 6 1.6 TDI"});
        Mockito.verify(mockPerson).assignSalaryPackage(50000, 10000, "VW Golf Mk 6 1.6 TDI");
    }
    
    @Test
    public void correctCall1Int1String() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "assignSalaryPackage", new Object[] {50000, "VW Golf Mk 6 1.6 TDI"});
        Mockito.verify(mockPerson).assignSalaryPackage(50000, "VW Golf Mk 6 1.6 TDI");
    }
    
    @Test
    public void correctCall1Double() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "ratePerformance", new Object[] { 4.5 });
        Mockito.verify(mockPerson).ratePerformance(4.5);
    }
    
    @Test
    public void correctCall1Double1String() throws Exception {
        methodCaller.callMethodWithParameters(mockPerson, "ratePerformance", new Object[] { 2.1, "needs to increase sales"});
        Mockito.verify(mockPerson).ratePerformance(2.1, "needs to increase sales");
    }
    
}
