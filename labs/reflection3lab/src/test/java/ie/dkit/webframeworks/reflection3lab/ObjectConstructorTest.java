package ie.dkit.webframeworks.reflection3lab;

import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.Before;

public class ObjectConstructorTest {
    
    private ObjectConstructor objectConstructor;
    
    @Before
    public void setup() {
        objectConstructor = new ObjectConstructor();
    }
    
    @After
    public void teardown() {
        objectConstructor = null;
    }
    
    @Test
    public void canUseDefaultConstructor() throws Exception {
        Person p = (Person) objectConstructor.constructObjectWithParameters("ie.dkit.webframeworks.reflection3lab.Person", new Object[] {});
        assertTrue(p.toString().contains("Doe") && p.toString().contains("John"));
    }
    
    @Test
    public void canUse2StringConstructor() throws Exception {
        Person p = (Person) objectConstructor.constructObjectWithParameters("ie.dkit.webframeworks.reflection3lab.Person", new Object[] { "Peter", "Gelb" });  
        assertTrue(p.toString().contains("Peter") && p.toString().contains("Gelb"));
    }
    
    @Test
    public void canUse2String1IntConstructor() throws Exception {
        Person p = (Person) objectConstructor.constructObjectWithParameters("ie.dkit.webframeworks.reflection3lab.Person", new Object[] { "Joyce", "Di Donato", 50000 });  
        assertTrue(p.toString().contains("Joyce") && p.toString().contains("Di Donato"));
    }
    
}
